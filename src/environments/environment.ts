// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const baseUrl = 'http://127.0.0.1:8000/api/';

export const environment = {
  production: false,
  authUrl: baseUrl + 'auth/',
  userUrl: baseUrl + 'users/',
  salonUrl: baseUrl + 'salons/',
  reviewUrl: baseUrl + 'reviews/',
  reservationUrl: baseUrl + 'reservations/',
  serviceUrl: baseUrl + 'services/',
  employeeUrl: baseUrl + 'employees/',
  googleApiKey: "AIzaSyAkuprHHLVa4VC7qzufpC328KTJIRNkpXs",
  googlePlacesUrl: "https://maps.googleapis.com/maps/api/place/details/",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
