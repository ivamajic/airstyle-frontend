import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const routes: Routes = [
/*  {
    path: 'login',
    component: LoginComponent
  },*/
  {
    path: '',
    loadChildren: '../app/core/layout/layout.module#LayoutModule',
  //  canActivate: [AuthGuard]
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
