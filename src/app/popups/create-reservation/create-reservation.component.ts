import { Component, OnInit } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormControl } from '@angular/forms';
import * as moment from "moment";

import { PopupsService } from '../popups.service';
import { Salon } from '../../modules/salon/salon.model';
import { SalonService } from '../../modules/salon/salon.service';
import { Reservation } from 'src/app/modules/salon/reservation.model';
import { AuthService } from 'src/app/core/auth/auth.service';
import { ReservationService } from 'src/app/modules/salon/reservation.service';

@Component({
  selector: 'app-create-reservation',
  templateUrl: './create-reservation.component.html',
  styleUrls: ['./create-reservation.component.scss']
})
export class CreateReservationComponent implements OnInit {
  public faTimes = faTimes;

  public salon: Salon;
  public salonReservations: Reservation[] = [];

  public createReservationForm = new FormGroup({
    startTime: new FormControl(''),
    endTime: new FormControl(''),
    notes: new FormControl(''),
    salonId: new FormControl(''),
    userId: new FormControl(''),
    employeeId: new FormControl(''),
    services: new FormControl(''),
  });

  public selectedServices: {
    id: number,
    name: string,
  }[] = [];

  public selectedDate: {
    year: number,
    month: number,
    day: number,
  };

  public multiConfig = {
    singleSelection: false,
    idField: "id",
    textField: "name",
    enableCheckAll: false,
    allowSearchFilter: true,
    searchPlaceholderText: "Search",
    defaultOpen: false,
    noDataAvailablePlaceholderText: "No services available, try again later.",
  };

  public totalDuration: number = 0;
  public totalPrice: number = 0;

  public freeTerms: {
    start: number,
    end: number,
    display: string,
  }[] = [];

  constructor(
    public _popups: PopupsService,
    public _salon: SalonService,
    public _auth: AuthService,
    private _reservation: ReservationService,
  ) { }

  ngOnInit() {
    this.getSalonById(this._popups.selectedSalonId);
    this.getSalonReservations(this._popups.selectedSalonId);
  }
  
  public getSalonById(id): void {
    this._salon.getById(id)
      .subscribe(data => {
        this.salon = data;
      })
  }

  public getSalonReservations(id): void {
    this._salon.getReservations(id)
      .subscribe(data => {
        this.salonReservations = data.map(reservation => {
          reservation.startTime = moment.utc(reservation.startTime);
          reservation.endTime = moment.utc(reservation.endTime);
          return reservation;
        });
      })
  }

  public onServiceSelect(event, deselecting): void {
    this.salon.services.forEach(service => {
      if (service.id === event.id) {
        deselecting ? this.totalDuration -= service.duration : this.totalDuration += service.duration;
        deselecting ? this.totalPrice -= service.price : this.totalPrice += service.price;
      }      
    });
    if (this.selectedDate) {
      this.calculateTime();
    }
  }

  public calculateTime(): void {
    this.freeTerms = [];
    const dateReservations = this.salonReservations.filter(reservation => reservation.startTime.year() === this.selectedDate.year && reservation.startTime.month() + 1 === this.selectedDate.month && reservation.startTime.date() === this.selectedDate.day);
    for (let i = this.salon.workHoursStart; i <= this.salon.workHoursEnd - this.totalDuration / 60; i += 0.25) {
      const potentialEnd = i + this.totalDuration / 60;
      let invalidFlag = false;
      dateReservations.forEach(reservation => {       
        if (i < reservation.endTime.hour() && potentialEnd > reservation.startTime.hour()) {
          invalidFlag = true;
        }
      });
      if (!invalidFlag) {
        const wholeStart = Math.floor(i);
        const decimalStart = (i - Math.floor(i)) * 60 === 0 ? "00" : Math.round((i - Math.floor(i)) * 60);
        const wholeEnd = Math.floor(potentialEnd);
        const decimalEnd = (potentialEnd - Math.floor(potentialEnd)) * 60 === 0 ? "00" : Math.round((potentialEnd - Math.floor(potentialEnd)) * 60);
        const term = {
          start: i,
          end: potentialEnd,
          display: wholeStart + ":" + decimalStart + " - " + wholeEnd + ":" + decimalEnd,
        }
        this.freeTerms.push(term);
      }
    }
  }

  public selectEmployee(employee): void {
    this.createReservationForm.patchValue({
      employeeId: employee.id,
    })
  }

  public selectTime(time): void {
    const start = moment.utc(this.selectedDate).subtract(1, "month").startOf("day").hour(Math.floor(time.start)).minute(60 * (time.start - Math.floor(time.start))).format("YYYY-MM-DD HH:mm:ss");
    const end = moment.utc(this.selectedDate).subtract(1, "month").startOf("day").hour(Math.floor(time.end)).minute(60 * (time.end - Math.floor(time.end))).format("YYYY-MM-DD HH:mm:ss");
    this.createReservationForm.patchValue({
      startTime: start,
      endTime: end,
    })
  }

  public createReservation(): void {
    this.createReservationForm.patchValue({
      salonId: this._popups.selectedSalonId,
      userId: this._auth.user.id,
      services: this.selectedServices.map(service => service.id),
    })
    this._reservation.create(this.createReservationForm.value)
      .subscribe(data => {
        this._popups.toggleCreateReservation();
      })
  }

}
