import { Component, OnInit, ViewChild } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormControl } from '@angular/forms';

import { PopupsService } from '../popups.service';
import { EmployeeService } from '../../modules/salon/employee.service';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.scss']
})
export class CreateEmployeeComponent implements OnInit {

  @ViewChild('firstName') private firstName;
  @ViewChild('lastName') private lastName;
  
  public faTimes = faTimes;
    
  public createEmployeeForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    salonId: new FormControl(''),
  });

  constructor(
    public _popups: PopupsService,
    public _employee: EmployeeService
  ) { }

  ngOnInit() {
  }

  public createEmployee(): void {
    this.createEmployeeForm.patchValue({
      firstName: this.firstName.input,
      lastName: this.lastName.input,
      salonId: this._popups.selectedSalonId,
    });
    this._employee.create(this.createEmployeeForm.value)
      .subscribe(() => {
        this._popups.toggleCreateEmployee();
        window.location.reload();
      });
  }

}
