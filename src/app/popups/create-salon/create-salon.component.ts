import { Component, OnInit, ViewChild } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormControl } from '@angular/forms';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

import { PopupsService } from '../popups.service';
import { SalonService } from 'src/app/modules/salon/salon.service';
import { UserService } from 'src/app/modules/user/user.service';
import { AuthService } from 'src/app/core/auth/auth.service';

@Component({
  selector: 'app-create-salon',
  templateUrl: './create-salon.component.html',
  styleUrls: ['./create-salon.component.scss']
})
export class CreateSalonComponent implements OnInit {
  public faTimes = faTimes;

  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  @ViewChild('name') name;
  @ViewChild('email') email;
  @ViewChild('phone') phone;

  public invalidAddressFlag: boolean = false;

  public createSalonForm = new FormGroup({
    name: new FormControl(''),
    workHoursStart: new FormControl(''),
    workHoursEnd: new FormControl(''),
    email: new FormControl(''),
    address: new FormControl(''),
    postCode: new FormControl(''),
    city: new FormControl(''),
    country: new FormControl(''),
    latitude: new FormControl(''),
    longitude: new FormControl(''),
    phone: new FormControl(''),
  });

  public locationOptions = {
    types: ['address']
  };

  constructor(
    public _popups: PopupsService,
    public _salon: SalonService,
    public _user: UserService,
    public _auth: AuthService,
  ) {
    this.createSalonForm.patchValue({
      workHoursStart: {
        hour: 8,
        minute: 0
      },
      workHoursEnd: {
        hour: 20,
        minute: 0
      }
    });
  }

  ngOnInit() {
  }

  public handleAddressChange(address: Address) {
    let street; let number; let postCode; let city; let country;
    address.address_components.forEach(item => {
      switch(item.types[0]) {
        case "street_number":
          number = item.long_name;
          break;
        case "route":
          street = item.long_name;
          break;
        case "locality":
          city = item.long_name;
          break;
        case "country":
          country = item.long_name;
          break;
        case "postal_code":
          postCode = item.long_name;
          break;
        default: break;
      }
    });

    if (!street || !number || !postCode || !city || !country) {
      this.invalidAddressFlag = true;
    } else {
      this.invalidAddressFlag = false;
      this.createSalonForm.patchValue({
        address: street + " " + number,
        postCode: postCode,
        city: city,
        country: country,
        latitude: address.geometry.location.lat(),
        longitude: address.geometry.location.lng(),
      })
    }
  }

  public createSalon() {
    const start = this.createSalonForm.value.workHoursStart;
    const end = this.createSalonForm.value.workHoursEnd;
    this.createSalonForm.patchValue({
      workHoursStart: start.hour + start.minute / 60,
      workHoursEnd: end.hour + end.minute / 60,
      name: this.name.input,
      email: this.email.input,
      phone: this.phone.input,
    });
    this._salon.create(this.createSalonForm.value)
      .subscribe(data => {
        this._user.update({salonId: data.id}, this._auth.user.id)
          .subscribe(dat => {
            this.createSalonForm.patchValue({
              workHoursStart: start,
              workHoursEnd: end,
            })
            this._popups.toggleCreateSalon();
            localStorage.setItem('user', JSON.stringify(dat));
          })
      })
  }
}
