import { Component, OnInit, ViewChild } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormControl } from '@angular/forms';
import { PopupsService } from '../popups.service';
import { ServiceService } from 'src/app/modules/salon/service.service';

@Component({
  selector: 'app-create-service',
  templateUrl: './create-service.component.html',
  styleUrls: ['./create-service.component.scss']
})
export class CreateServiceComponent implements OnInit {

  @ViewChild('name') private name;
  @ViewChild('price') private price;
  @ViewChild('duration') private duration;

  public faTimes = faTimes;

  public createServiceForm = new FormGroup({
    name: new FormControl(''),
    price: new FormControl(''),
    duration: new FormControl(''),
    salonId: new FormControl(''),
  });

  constructor(
    public _popups: PopupsService,
    public _service: ServiceService
  ) { }

  ngOnInit() {
  }

  public createService(): void {
    this.createServiceForm.patchValue({
      name: this.name.input,
      price: this.price.input,
      duration: this.duration.input,
      salonId: this._popups.selectedSalonId,
    });
    this._service.create(this.createServiceForm.value)
      .subscribe(() => {
        this._popups.toggleCreateService();
        window.location.reload();
      });
  }

}