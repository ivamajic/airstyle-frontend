import { Component, OnInit, ViewChild } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormControl } from '@angular/forms';

import { PopupsService } from '../popups.service';
import { UserService } from 'src/app/modules/user/user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  @ViewChild('email') email;
  @ViewChild('firstName') firstName;
  @ViewChild('lastName') lastName;
  @ViewChild('password') password;
  @ViewChild('confirmPassword') confirmPassword;

  public faTimes = faTimes;
  public passwordFlag: boolean = false;

  public signupForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
    firstName: new FormControl(''),
    lastName: new FormControl('')
  });

  constructor(
    public _popups: PopupsService,
    public _user: UserService
  ) {
   }

  ngOnInit() {
  }

  signup() {
    if (this.password.input === this.confirmPassword.input) {
       this.passwordFlag = false;
       this.signupForm.setValue({
         email: this.email.input,
         password: this.password.input,
         firstName: this.firstName.input,
         lastName: this.lastName.input
       });
       this._user.create(this.signupForm.value)
        .subscribe(
          data => {
            localStorage.setItem('user', JSON.stringify(data));
            this._popups.toggleSignup();
          }
        )
    } else {
      this.passwordFlag = true;
    }
  }

}
