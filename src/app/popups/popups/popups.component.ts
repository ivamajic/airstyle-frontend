import { Component, OnInit } from '@angular/core';

import { PopupsService } from '../popups.service';

@Component({
  selector: 'app-popups',
  templateUrl: './popups.component.html',
  styleUrls: ['./popups.component.scss']
})
export class PopupsComponent implements OnInit {

  constructor(
    public _popups: PopupsService
  ) { }

  ngOnInit() {
  }

}
