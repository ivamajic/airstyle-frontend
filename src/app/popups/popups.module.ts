import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbTimepickerModule, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { SignupComponent } from './signup/signup.component';
import { SharedModule } from '../shared/shared.module';
import { PopupsComponent } from './popups/popups.component';
import { PopupsService } from './popups.service';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserModule } from '../modules/user/user.module';
import { CreateSalonComponent } from './create-salon/create-salon.component';
import { CreateServiceComponent } from './create-service/create-service.component';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { CreateReviewComponent } from './create-review/create-review.component';
import { CreateReservationComponent } from './create-reservation/create-reservation.component';

@NgModule({
  declarations: [SignupComponent, PopupsComponent, LoginComponent, CreateSalonComponent, CreateServiceComponent, CreateEmployeeComponent, CreateReviewComponent, CreateReservationComponent],
  imports: [
    CommonModule,
    SharedModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    UserModule,
    NgbTimepickerModule,
    NgbDatepickerModule,
    GooglePlaceModule,
    NgMultiSelectDropDownModule.forRoot()
  ],
  providers: [
    PopupsService,
  ],
  exports: [PopupsComponent],
})
export class PopupsModule { }
