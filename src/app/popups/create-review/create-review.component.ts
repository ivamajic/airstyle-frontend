import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { PopupsService } from '../popups.service';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/core/auth/auth.service';
import { InputComponent } from 'src/app/shared/input/input.component';
import { ReviewService } from 'src/app/modules/salon/review.service';

@Component({
  selector: 'app-create-review',
  templateUrl: './create-review.component.html',
  styleUrls: ['./create-review.component.scss']
})
export class CreateReviewComponent implements OnInit {
  public faTimes = faTimes;

  @ViewChild('title') public title: InputComponent;
  @ViewChild('description') public description: ElementRef;


  public createReviewForm = new FormGroup({
    title: new FormControl(''),
    description: new FormControl(''),
    rating: new FormControl(''),
    salonId: new FormControl(''),
    userId: new FormControl(''),
  });


  constructor(
    public _popups: PopupsService,
    public _auth: AuthService,
    public _review: ReviewService,
  ) { }

  ngOnInit() {
  }

  public createReview(): void {
    this.createReviewForm.patchValue({
      userId: this._auth.user.id,
      salonId: this._popups.selectedSalonId,
      rating: this._popups.selectedRating,
      title: this.title.input,
      description: this.description.nativeElement.value,
    });
    this._review.create(this.createReviewForm.value)
      .subscribe(() => {
        this._popups.toggleCreateReview();
        window.location.reload();
      })
  }

}
