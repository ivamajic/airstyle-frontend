import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PopupsService {

  public signup: boolean = false;
  public login: boolean = false;
  public createSalon: boolean = false;
  public createEmployee: boolean = false;
  public createService: boolean = false;
  public createReview: boolean = false;
  public createReservation: boolean = false;

  public selectedSalonId: number;
  public selectedRating: number;

  constructor() { }

  public toggleSignup() {
    this.signup = !this.signup;
  }

  public toggleLogin() {
    this.login = !this.login;
  }

  public toggleCreateSalon() {
    this.createSalon = !this.createSalon;
  }

  public toggleCreateEmployee() {
    this.createEmployee = !this.createEmployee;
  }

  public toggleCreateService() {
    this.createService = !this.createService;
  }

  public toggleCreateReview() {
    this.createReview = !this.createReview;
    if (!this.createReview) {
      this.selectedRating = null;
    }
  }

  public toggleCreateReservation() {
    this.createReservation = !this.createReservation;
  }
}
