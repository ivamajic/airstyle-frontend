import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import { PopupsService } from '../popups.service';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/core/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('email') email;
  @ViewChild('password') password;


  public faTimes = faTimes;

  public loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
    remember_me: new FormControl(''),
  });

  constructor(
    public _popups: PopupsService,
    public _auth: AuthService,
  ) { }

  ngOnInit() { }

  public login(): void {
    this.loginForm.patchValue({
      email: this.email.input,
      password: this.password.input,
    });
    this._auth.login(this.loginForm.value)
      .subscribe(
        data => {
          localStorage.setItem('user', JSON.stringify(data));
          this._popups.toggleLogin();
        }
      )
  }

}
