import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { AuthService } from './auth.service';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(
        private _auth: AuthService,
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this._auth.user) {
            req = req.clone({
                setHeaders: {
                    'Authorization': `Bearer ${this._auth.user.accessToken}`,
                },
            });
        }
        return next.handle(req);
    }
}