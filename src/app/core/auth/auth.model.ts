import { Salon } from 'src/app/modules/salon/salon.model';

export class Auth {
    id: number;
    firstName: string;
    lastName: string;
    image: string;
    role: string;
    email: string;
    created_at: string;
    updated_at: string;
    salonId: number;
    accessToken: string;
    tokenExpiresAt: string;
    
    constructor() {
    }
  
     get name() {
      return this.firstName && this.lastName ? this.firstName + ' ' + this.lastName : null;
    }
  }
  