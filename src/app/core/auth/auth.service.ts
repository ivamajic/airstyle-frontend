import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../../environments/environment';
import { map } from 'rxjs/operators';


import { Auth } from './auth.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  /** Backend URL. */
  private url: string = environment.authUrl;

  constructor(
    private http: HttpClient
  ) { }

  public login(formData): Observable<Auth> {
    const form = new FormData();
    form.append('email', formData.email);
    form.append('password', formData.password);
    return this.http.post<Auth>(this.url + 'login', form)
      .pipe(
        map(data => {
          const obj = new Auth();
          Object.assign(obj, data);
          return obj;
        })
      )
  }

  get user() {
    return JSON.parse(localStorage.getItem('user'));
  }

  public logout(): Observable<any> {
    return this.http.get<any>(this.url + 'logout');
  }
}
