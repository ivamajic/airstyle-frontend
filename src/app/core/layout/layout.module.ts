import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { HomeModule } from 'src/app/modules/home/home.module';
import { NavigationComponent } from './header/navigation/navigation.component';
import { PopupsModule } from 'src/app/popups/popups.module';
import { SalonModule } from 'src/app/modules/salon/salon.module';
import { UserModule } from 'src/app/modules/user/user.module';

@NgModule({
  declarations: [HeaderComponent, MainComponent, NavigationComponent],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    HomeModule,
    PopupsModule,
    SalonModule,
    UserModule,
  ]
})
export class LayoutModule { }
