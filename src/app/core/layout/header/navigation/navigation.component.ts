import { Component, OnInit } from '@angular/core';

import { AuthService } from 'src/app/core/auth/auth.service';
import { PopupsService } from 'src/app/popups/popups.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(
    public _auth: AuthService,
    public _popups: PopupsService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  public logout(): void {
    this._auth.logout()
      .subscribe(
        () => {
          localStorage.removeItem('user');
          this.router.navigate(['']);
        }
      );
  }
}
