import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './main/main.component';
import { HomeComponent } from 'src/app/modules/home/home/home.component';
import { SalonsComponent } from 'src/app/modules/salon/salons/salons.component';
import { ManageSalonComponent } from 'src/app/modules/salon/manage-salon/manage-salon.component';
import { AuthGuard } from '../auth/auth.guard';
import { SalonComponent } from 'src/app/modules/salon/salon/salon.component';
import { ProfileComponent } from 'src/app/modules/user/profile/profile.component';


const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        component: HomeComponent,
      },
      {
        path: 'salons',
        component: SalonsComponent
      },
      {
        path: 'manage',
        component: ManageSalonComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'salon/:id',
        component: SalonComponent
      },
      {
        path: 'profile',
        component: ProfileComponent,
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
  ]
})
export class LayoutRoutingModule { }
