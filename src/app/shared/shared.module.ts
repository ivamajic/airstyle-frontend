import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { InputComponent } from './input/input.component';
import { DropdownComponent } from './dropdown/dropdown.component';

@NgModule({
  declarations: [InputComponent, DropdownComponent],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [
    InputComponent,
    DropdownComponent,
  ],
})
export class SharedModule { }
