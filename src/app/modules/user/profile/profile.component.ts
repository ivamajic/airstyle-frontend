import { Component, OnInit } from '@angular/core';
import * as moment from "moment";
import { faCaretRight, faCaretDown, faTrash } from '@fortawesome/free-solid-svg-icons';

import { Auth } from 'src/app/core/auth/auth.model';
import { AuthService } from 'src/app/core/auth/auth.service';
import { Reservation } from '../../salon/reservation.model';
import { UserService } from '../user.service';
import { ReservationService } from '../../salon/reservation.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public faCaretRight = faCaretRight;
  public faCaretDown = faCaretDown;
  public faTrash = faTrash;

  public user: Auth;

  public reservations: Reservation[];

  constructor(
    private _auth: AuthService,
  ) { }

  ngOnInit() {
    this.user = this._auth.user;
  }

}
