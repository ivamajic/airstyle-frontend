import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../../environments/environment';
import { map } from 'rxjs/operators';

import { Auth } from 'src/app/core/auth/auth.model';
import { Reservation } from 'src/app/modules/salon/reservation.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  /** Backend URL. */
  private url: string = environment.userUrl;

  constructor(
    private http: HttpClient
  ) { }

  public create(formData): Observable<Auth> {
    return this.http.post<Auth>(this.url, formData)
    .pipe(
      map(data => {
        const obj = new Auth();
        Object.assign(obj, data);
        return obj;
      })
    )
  }

  public update(formData, userId): Observable<Auth> {
    return this.http.put<Auth>(this.url + userId, formData)
    .pipe(
      map(data => {
        const obj = new Auth();
        Object.assign(obj, data);
        return obj;
      })
    )
  }

  public getReservations(id): Observable<Reservation[]> {
    return this.http.get<Reservation[]>(this.url + id + "/reservations")
      .pipe(
        map(data => {
          const reservations = [];
          data.forEach(reservation => {
            reservations.push(Object.assign(new Reservation(), reservation));
          });
          return reservations;
        })
      );
  }

}
