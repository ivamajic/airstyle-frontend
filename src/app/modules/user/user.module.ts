import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RouterModule } from '@angular/router';

import { UserService } from './user.service';
import { ProfileComponent } from './profile/profile.component';
import { SalonModule } from '../salon/salon.module';

@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    RouterModule,
    SalonModule
  ],
  providers: [
    UserService,
  ],
})
export class UserModule { }
