import { Salon } from './salon.model';
import { Auth } from 'src/app/core/auth/auth.model';

export class Review {
    id: number;
    created_at: string;
    updated_at: string;
    title: string;
    description: string;
    rating: number;
    salon: Salon;
    user: Auth;
}