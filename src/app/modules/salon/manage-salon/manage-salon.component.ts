import { Component, OnInit } from '@angular/core';

import { AuthService } from 'src/app/core/auth/auth.service';
import { PopupsService } from 'src/app/popups/popups.service';

@Component({
  selector: 'app-manage-salon',
  templateUrl: './manage-salon.component.html',
  styleUrls: ['./manage-salon.component.scss']
})
export class ManageSalonComponent implements OnInit {

  constructor(
    public _auth: AuthService,
  ) { }

  ngOnInit() {
  }

}
