import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageSalonComponent } from './manage-salon.component';

describe('ManageSalonComponent', () => {
  let component: ManageSalonComponent;
  let fixture: ComponentFixture<ManageSalonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageSalonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageSalonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
