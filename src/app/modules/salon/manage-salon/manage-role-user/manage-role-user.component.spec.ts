import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageRoleUserComponent } from './manage-role-user.component';

describe('ManageRoleUserComponent', () => {
  let component: ManageRoleUserComponent;
  let fixture: ComponentFixture<ManageRoleUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageRoleUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageRoleUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
