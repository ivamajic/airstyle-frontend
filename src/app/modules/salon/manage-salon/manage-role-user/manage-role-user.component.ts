import { Component, OnInit } from '@angular/core';
import { PopupsService } from '../../../../popups/popups.service';

@Component({
  selector: 'app-manage-role-user',
  templateUrl: './manage-role-user.component.html',
  styleUrls: ['./manage-role-user.component.scss']
})
export class ManageRoleUserComponent implements OnInit {

  constructor(
    public _popups: PopupsService,
    ) { }

  ngOnInit() {
  }

}
