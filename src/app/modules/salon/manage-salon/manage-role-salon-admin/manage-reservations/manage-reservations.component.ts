import { Component, OnInit, Input } from '@angular/core';
import { faCaretRight, faCaretLeft, faTrash } from '@fortawesome/free-solid-svg-icons';
import * as moment from "moment";
import { Router } from '@angular/router';

import { SalonService } from '../../../salon.service';
import { Salon } from '../../../salon.model';
import { Reservation } from '../../../reservation.model';
import { ReservationService } from '../../../reservation.service';
import { AuthService } from 'src/app/core/auth/auth.service';
import { UserService } from 'src/app/modules/user/user.service';

@Component({
  selector: 'app-manage-reservations',
  templateUrl: './manage-reservations.component.html',
  styleUrls: ['./manage-reservations.component.scss']
})
export class ManageReservationsComponent implements OnInit {

  @Input() public salonId: number;
  private salon: Salon;

  public faCaretRight = faCaretRight;
  public faCaretLeft = faCaretLeft;
  public faTrash = faTrash;

  public startDate = moment.utc().startOf("week").add(1, "day");
  public endDate = moment.utc().endOf("week").add(1, "day");
  private currentWeek: number = 0;
  public dates: {
    date,
    reservations
  }[] = [];
  public timeIntervals = [];
  private reservations: Reservation[] = [];

  constructor(
    private _salon: SalonService,
    private _reservation: ReservationService,
    private _auth: AuthService,
    private _user: UserService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.fillDates();
    if (this.router.url === "/profile") {
      this.getUserReservations();
    } else {
      this.getSalon();
    }
  }

  public getUserReservations(): void {
    this._user.getReservations(this._auth.user.id)
    .subscribe( data => {
      this.reservations = data;
      this.reservations.forEach(reservation => {
        this.timeIntervals = [];
        for (let i = 0; i < 24; i += 0.25) {
          this.timeIntervals.push({
            time: i,
            hours: Math.floor(i),
            minutes: (i - Math.floor(i)) * 60 < 10 ? "0" + (i - Math.floor(i)) * 60 : (i - Math.floor(i)) * 60,
          });
        }
        this.mapReservations();
      });
    })
  }

  public getSalonReservations(): void {    
    this._salon.getReservations(this.salonId)
      .subscribe(data => {
        this.reservations = data;
        this.mapReservations();
      })
  }

  private mapReservations(): void {
    this.dates.forEach(date => {
      date.reservations = this.reservations
        .filter(reservation => moment.utc(reservation.startTime).format("YYYY/MM/DD") === date.date.format("YYYY/MM/DD"))
        .map(reservation => {
          const startHours = moment.utc(reservation.startTime).hours();
          const startMinutes = moment.utc(reservation.startTime).minutes();
          const endHours = moment.utc(reservation.endTime).hours();
          const endMinutes = moment.utc(reservation.endTime).minutes();
          const start = startHours + startMinutes / 60;
          const end = endHours + endMinutes / 60;
          const noIntervalsHeight = (end - start) / 0.25;
          const noIntervalsTop = this.router.url === "/profile" ? start / 0.25 : (start - this.salon.workHoursStart) / 0.25;
          reservation.height = noIntervalsHeight * 30;
          reservation.top = noIntervalsTop * 30 + 30;
          reservation.startHours = (startHours < 10 ? "0" + startHours : startHours) + ":" + (startMinutes < 10 ? "0" + startMinutes : startMinutes);
          reservation.endHours = (endHours < 10 ? "0" + endHours : endHours) + ":" + (endMinutes < 10 ? "0" + endMinutes : endMinutes);
          reservation.isInFuture = moment.utc(reservation.startTime).isAfter(moment.utc().add(2, "hour"));
          return reservation;
        })
    })
  }

  public getSalon(): void {
    this._salon.getById(this.salonId)
      .subscribe(data => {
        this.salon = data;
        for (let i = this.salon.workHoursStart; i <= this.salon.workHoursEnd; i += 0.25) {
          this.timeIntervals.push({
            time: i,
            hours: Math.floor(i),
            minutes: (i - Math.floor(i)) * 60 < 10 ? "0" + (i - Math.floor(i)) * 60 : (i - Math.floor(i)) * 60,
          });
        }
        this.getSalonReservations();
      })
  }

  public previousWeek(): void {
    this.currentWeek -= 1;
    this.fillDates();
  }

  public nextWeek(): void {
    this.currentWeek += 1;
    this.fillDates();
  }

  public thisWeek(): void {
    this.currentWeek = 0;
    this.fillDates();    
  }

  private fillDates(): void {
    this.startDate = moment.utc().add(this.currentWeek, "week").startOf("week").add(1, "day");
    this.endDate = moment.utc().add(this.currentWeek, "week").endOf("week").add(1, "day");
    this.dates = [];
    for (let i = 0; i < 7; i++) {
      const date = this.startDate.clone().add(i, "day")
      this.dates.push({
        date: date,
        reservations: [],
      })
    }
    this.mapReservations();
  }

  public deleteReservation(e, id): void {
    e.stopPropagation();
    this._reservation.delete(id)
      .subscribe(data => {
        this.router.url === "/profile" ? this.getUserReservations() : this.getSalonReservations();
      })
  }

}
