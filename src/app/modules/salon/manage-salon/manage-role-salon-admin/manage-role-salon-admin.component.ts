import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { InputComponent } from 'src/app/shared/input/input.component';

import { Salon } from '../../salon.model';
import { PopupsService } from '../../../../popups/popups.service';
import { SalonService } from '../../salon.service';

@Component({
  selector: 'app-manage-role-salon-admin',
  templateUrl: './manage-role-salon-admin.component.html',
  styleUrls: ['./manage-role-salon-admin.component.scss']
})
export class ManageRoleSalonAdminComponent implements OnInit {

  @Input() public salonId: number;
  private salon: Salon;

  public tabOpen: number = 1;
  public invalidAddressFlag: boolean = false;

  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  @ViewChild('name') name: InputComponent;
  @ViewChild('email') email: InputComponent;
  @ViewChild('phone') phone: InputComponent;

  public updateSalonForm = new FormGroup({
    name: new FormControl(''),
    workHoursStart: new FormControl(''),
    workHoursEnd: new FormControl(''),
    email: new FormControl(''),
    address: new FormControl(''),
    postCode: new FormControl(''),
    city: new FormControl(''),
    country: new FormControl(''),
    latitude: new FormControl(''),
    longitude: new FormControl(''),
    phone: new FormControl(''),
  });

  constructor(
    public _popups: PopupsService,
    public _salon: SalonService,
  ) {
  }

  ngOnInit() {
    this._popups.selectedSalonId = this.salonId;
    this.getSalon();
  }

  public getSalon(): void {
    this._salon.getById(this.salonId)
      .subscribe(data => {
        this.salon = data;
        this.updateSalonForm.patchValue({
          workHoursStart: {
            hour: Math.floor(this.salon.workHoursStart),
            minute: (this.salon.workHoursStart - Math.floor(this.salon.workHoursStart)) * 60
          },
          workHoursEnd: {
            hour: Math.floor(this.salon.workHoursEnd),
            minute: (this.salon.workHoursEnd - Math.floor(this.salon.workHoursEnd)) * 60
          }
        });
        this.name.input = this.salon.name;
        this.email.input = this.salon.email;
        this.phone.input = this.salon.phone;
      });
  }

  public handleAddressChange(address: Address) {
    if (address.address_components.length < 7) {
      this.invalidAddressFlag = true;
    } else {
      this.invalidAddressFlag = false;
      this.updateSalonForm.patchValue({
        address: address.address_components[1].long_name + " " + address.address_components[0].long_name,
        postCode: address.address_components[6].long_name,
        city: address.address_components[2].long_name,
        country: address.address_components[5].long_name,
        latitude: address.geometry.location.lat(),
        longitude: address.geometry.location.lng(),
      })
    }
  }

  public updateSalon() {
    const start = this.updateSalonForm.value.workHoursStart;
    const end = this.updateSalonForm.value.workHoursEnd;
    this.updateSalonForm.patchValue({
      workHoursStart: start.hour + start.minute / 60,
      workHoursEnd: end.hour + end.minute / 60,
      name: this.name.input,
      email: this.email.input,
      phone: this.phone.input,
    });
    this._salon.update(this.updateSalonForm.value, this.salonId)
      .subscribe(data => {
      })
  }
}
