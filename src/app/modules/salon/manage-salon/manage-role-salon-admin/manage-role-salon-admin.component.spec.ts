import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageRoleSalonAdminComponent } from './manage-role-salon-admin.component';

describe('ManageRoleSalonAdminComponent', () => {
  let component: ManageRoleSalonAdminComponent;
  let fixture: ComponentFixture<ManageRoleSalonAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageRoleSalonAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageRoleSalonAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
