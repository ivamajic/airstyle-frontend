import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageRoleSysAdminComponent } from './manage-role-sys-admin.component';

describe('ManageRoleSysAdminComponent', () => {
  let component: ManageRoleSysAdminComponent;
  let fixture: ComponentFixture<ManageRoleSysAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageRoleSysAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageRoleSysAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
