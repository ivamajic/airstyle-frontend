import { Component, OnInit } from '@angular/core';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

import { SalonService } from '../../salon.service';
import { Salon } from '../../salon.model';

@Component({
  selector: 'app-manage-role-sys-admin',
  templateUrl: './manage-role-sys-admin.component.html',
  styleUrls: ['./manage-role-sys-admin.component.scss']
})
export class ManageRoleSysAdminComponent implements OnInit {

  public salons: Salon[] = [];
  public faArrowLeft = faArrowLeft;

  constructor(
    private _salon: SalonService
  ) { }

  ngOnInit() {
    this.getAllSalons();
  }

  public getAllSalons(): void {
    this._salon.getAllSalons()
      .subscribe(data => {
        this.salons = data;
      })
  }

}
