import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { faStar, faMapMarkerAlt, faEnvelope, faPhone } from '@fortawesome/free-solid-svg-icons';

import { SalonService } from '../salon.service';
import { Salon } from '../salon.model';
import { Review } from '../review.model';
import { PopupsService } from 'src/app/popups/popups.service';

@Component({
  selector: 'app-salon',
  templateUrl: './salon.component.html',
  styleUrls: ['./salon.component.scss']
})
export class SalonComponent implements OnInit {

  public faStar = faStar;
  public faMapMarkerAlt = faMapMarkerAlt;
  public faEnvelope = faEnvelope;
  public faPhone = faPhone;

  public salon: Salon;
  public reviews: Review[] = [];
  public sorting: string = "created_at";

  constructor(
    private _salon: SalonService,
    private route: ActivatedRoute,
    public _popups: PopupsService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(data => {
      this.getSalonById(data.id);
      this.getSalonReviews(data.id);
    })
  }

  public getSalonById(id): void {
    this._salon.getById(id)
      .subscribe(data => {
        this.salon = data;
      })
  }

  public getSalonReviews(id): void {
    this._salon.getReviews(id)
      .subscribe(data => {
        this.reviews = data;
        this.sortReviewsBy("updated_at", true);
      })
  }

  public sortReviewsBy(key, asc?): void {
    this.sorting = key;
    if (asc) {
      this.reviews.sort((a, b) => (a[key] < b[key]) ? 1 : -1);
    } else {
      this.reviews.sort((a, b) => (a[key] > b[key]) ? 1 : -1);
    }
  }

  public rateSalon(rating): void {
    this._popups.selectedRating = rating;
    this._popups.selectedSalonId = this.salon.id;
    this._popups.toggleCreateReview();    
  }

  public bookAppointment(): void {
    this._popups.selectedSalonId = this.salon.id;
    this._popups.toggleCreateReservation();
  }
}
