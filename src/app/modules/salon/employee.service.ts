import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../../environments/environment';
import { map } from 'rxjs/operators';

import { Auth } from 'src/app/core/auth/auth.model';
import { Employee } from './employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  /** Backend URL. */
  private url: string = environment.employeeUrl;

  constructor(
    private http: HttpClient
  ) { }

  public create(formData): Observable<Employee> {
    return this.http.post<Employee>(this.url, formData)
    .pipe(
      map(data => {
        const obj = new Employee();
        Object.assign(obj, data);
        return obj;
      })
    )
  }

  public update(formData, employeeId): Observable<Employee> {
    return this.http.put<Employee>(this.url + employeeId, formData)
    .pipe(
      map(data => {
        const obj = new Employee();
        Object.assign(obj, data);
        return obj;
      })
    )
  }
}
