import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../../environments/environment';
import { map } from 'rxjs/operators';

import { Review } from './review.model';


@Injectable({
  providedIn: 'root'
})
export class ReviewService {
  /** Backend URL. */
  private url: string = environment.reviewUrl;

  constructor(
    private http: HttpClient
  ) { }

  public create(formData): Observable<Review> {
    return this.http.post<Review>(this.url, formData)
    .pipe(
      map(data => {
        const obj = new Review();
        Object.assign(obj, data);
        return obj;
      })
    )
  }
}
