import { Component, OnInit } from '@angular/core';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

import { Salon } from '../salon.model';
import { SalonService } from '../salon.service';

@Component({
  selector: 'app-salons',
  templateUrl: './salons.component.html',
  styleUrls: ['./salons.component.scss']
})
export class SalonsComponent implements OnInit {

  public faStar = faStar;

  public salons: Salon[] = [];
  private latitude: number;
  private longitude: number;
  public locationRejected: boolean = false;
  public sorting: string = "distance";
  
  constructor(
    private _salon: SalonService,
    public router: Router
  ) { }

  ngOnInit() {
    this.getAllSalons();
  }

  public getAllSalons(): void {
    this._salon.getAllSalons()
      .subscribe(data => {
        this.salons = data;

        navigator.geolocation.getCurrentPosition(response => {
          this.latitude = response.coords.latitude;
          this.longitude = response.coords.longitude;
          this.salons.map(salon => {
            salon.distance = Math.sqrt(Math.pow((salon.latitude - this.latitude), 2) + Math.pow((salon.longitude - this.longitude), 2));
            return salon;
          });
          this.sortSalonsBy("distance");
        }, () => {
          this.locationRejected = true;
          this.sortSalonsBy("name");
        });
      });
  }

  public sortSalonsBy(key, asc?): void {
    this.sorting = key;
    if (asc) {
      this.salons.sort((a, b) => (a[key] < b[key]) ? 1 : -1);
    } else {
      this.salons.sort((a, b) => (a[key] > b[key]) ? 1 : -1);
    }
  }

}
