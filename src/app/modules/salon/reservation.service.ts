import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../../environments/environment';
import { map } from 'rxjs/operators';

import { Reservation } from './reservation.model';


@Injectable({
  providedIn: 'root'
})
export class ReservationService {
  /** Backend URL. */
  private url: string = environment.reservationUrl;

  constructor(
    private http: HttpClient
  ) { }

  public create(formData): Observable<Reservation> {
    return this.http.post<Reservation>(this.url, formData)
    .pipe(
      map(data => {
        const obj = new Reservation();
        Object.assign(obj, data);
        return obj;
      })
    )
  }

  public delete(id): Observable<Reservation> {
    return this.http.delete<Reservation>(this.url + id)
    .pipe(
      map(data => {
        const obj = new Reservation();
        Object.assign(obj, data);
        return obj;
      })
    )
  }
}
