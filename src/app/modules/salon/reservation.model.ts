import { Auth } from 'src/app/core/auth/auth.model';
import { Salon } from './salon.model';
import { Employee } from './employee.model';
import { Service } from './service.model';

export class Reservation {
    id: number;
    startTime:  any;
    endTime: any;
    notes: string;
    user: Auth;
    salon: Salon;
    employee: Employee;
    services: Service[];
    updated_at: string;
    created_at: string;
    expanded?: boolean;
    startDate?: string;
    title?: string;
    isInFuture?: boolean;
    height?: number;
    top?: number;
    startHours?: string;
    endHours?: string;
}