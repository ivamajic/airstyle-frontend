export class Employee {
        id: number;
        created_at: string;
        updated_at: string;
        firstName: string;
        lastName: string;
        image: string;
        rating: number;
        noRatings: number;
        salonId: number;
}