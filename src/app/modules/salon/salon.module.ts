import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { SalonsComponent } from './salons/salons.component';
import { ManageSalonComponent } from './manage-salon/manage-salon.component';
import { SalonService } from './salon.service';
import { ManageRoleUserComponent } from './manage-salon/manage-role-user/manage-role-user.component';
import { ManageRoleSalonAdminComponent } from './manage-salon/manage-role-salon-admin/manage-role-salon-admin.component';
import { ManageRoleSysAdminComponent } from './manage-salon/manage-role-sys-admin/manage-role-sys-admin.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SalonComponent } from './salon/salon.component';
import { ReviewService } from './review.service';
import { ReservationService } from './reservation.service';
import { ManageReservationsComponent } from './manage-salon/manage-role-salon-admin/manage-reservations/manage-reservations.component';

@NgModule({
  declarations: [SalonsComponent, ManageSalonComponent, ManageRoleUserComponent, ManageRoleSalonAdminComponent, ManageRoleSysAdminComponent, SalonComponent, ManageReservationsComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgbTimepickerModule,
    GooglePlaceModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
  ],
  exports: [
    ManageReservationsComponent,
  ],
  providers: [
    SalonService,
    ReviewService,
    ReservationService
  ]
})
export class SalonModule { }
