import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Service } from './service.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  /** Backend URL. */
  private url: string = environment.serviceUrl;

  constructor(
    private http: HttpClient
  ) { }


  public create(formData): Observable<Service> {
    return this.http.post<Service>(this.url, formData)
      .pipe(
        map(data => {
          const obj = new Service();
          Object.assign(obj, data);
          return obj;
        })
      )
  }
}
