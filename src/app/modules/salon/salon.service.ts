import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../../environments/environment';
import { map } from 'rxjs/operators';

import { Salon } from './salon.model';
import { Review } from './review.model';
import { Reservation } from './reservation.model';

@Injectable({
  providedIn: 'root'
})
export class SalonService {

  /** Backend URL. */
  private url: string = environment.salonUrl;

  constructor(
    private http: HttpClient
  ) { }

  public create(formData): Observable<Salon> {
    return this.http.post<Salon>(this.url, formData)
    .pipe(
      map(data => {
        const obj = new Salon();
        Object.assign(obj, data);
        return obj;
      })
    )
  }

  public update(formData, id): Observable<Salon> {
    return this.http.put<Salon>(this.url + id, formData)
    .pipe(
      map(data => {
        const obj = new Salon();
        Object.assign(obj, data);
        return obj;
      })
    )
  }

  public getAllSalons(): Observable<Salon[]> {
    return this.http.get<Salon[]>(this.url)
      .pipe(
        map(data => {
          const salons = [];
          data.forEach(salon => {
            salons.push(Object.assign(new Salon(), salon));
          });
          return salons;
        })
      );
  }

  public getById(id): Observable<Salon> {
    return this.http.get<Salon>(this.url + id) 
      .pipe(
        map(data => {
          const obj = new Salon();
          Object.assign(obj, data);
          return obj;
        })
      )
  }

  public getReviews(id): Observable<Review[]> {
    return this.http.get<Review[]>(this.url + id + "/reviews")
    .pipe(
      map(data => {
        const reviews = [];
        data.forEach(review => {
          reviews.push(Object.assign(new Review(), review));
        });
        return reviews;
      })
    );
  }

  public getReservations(id): Observable<Reservation[]> {
    return this.http.get<Reservation[]>(this.url + id + "/reservations")
    .pipe(
      map(data => {
        const reservations = [];
        data.forEach(reservation => {
          reservations.push(Object.assign(new Reservation(), reservation));
        });
        return reservations;
      })
    );
  }
}
