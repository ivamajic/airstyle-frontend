import { Employee } from './employee.model';
import { Service } from './service.model';

export class Salon {
        id: number;
        created_at: string;
        updated_at: string;
        image: string;
        name: string;
        workHoursStart: number;
        workHoursEnd: number;
        email: string;
        address: string;
        postCode: number;
        city: string;
        latitude: number;
        longitude: number;
        phone: string;
        rating: number;
        noRatings: number;
        employees: Employee[];
        services: Service[];
        distance: number;
}